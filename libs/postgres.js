const {Client} = require('pg');

async function getConnection(){
  const client = new Client({
    host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'qwerty',
    database: 'my_store'
  });
  await client.connect();
  return client;
}

module.exports = getConnection;

// Para conectar con Postgres instalar, se creo carpeta libs/postgres.js
// npm i pg


