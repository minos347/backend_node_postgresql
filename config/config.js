require('dotenv').config();

const config = {
  env: process.env.NODE_ENV || 'dev',
  port: process.env.PORT || 3000,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORD,
  dbHost: process.env.DB_HOST,
  dbName: process.env.DB_NAME,
  dbPort: process.env.DB_PORT,
}

module.exports = {config};

// Es mecesario instalar la libreria para visualizar los cambios en el .env
// npm i dotenv

// se instalara al ultimo otro tipo de libreria que mejore la coneccion
// npm i --save sequelize
// instalacion de driver
// npm i --save pg pg-hstore

